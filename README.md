# Rubins_Rules

Rubins Rules for python. 
This code allows you to pool estimates according to Rubins Rules. 

Source:
Rubin, D.B. (1987). Multiple Imputation for Nonresponse in Surveys. New York: John Wiley and Sons.

plot-misc can be installed pip installed to a specific conda environment by
first activating the desired environment.

```
conda activate ENVIRONMENT_NAME
# update your packages using conda
conda env update --file resources/conda_env/conda_update.yml
# install plot-misc , make sure to direct your terminal to the package site
python -m pip install -e .
```


## Contributing
All contributions (additions or bug fixes) are more then welcome

## Authors and acknowledgment
I want to thank A. Floriaan Schmidt for his guidance during this project.


## Project status
Under construction!
