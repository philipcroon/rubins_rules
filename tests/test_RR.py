import random
import pandas as pd
from Rubins_Rules.module import *
import Rubins_Rules.example_data.examples as examples

# load data
# TODO add r in python. Not compatible with m1 chip... add later

file_list = examples.load_test_data()

file_list_time = file_list.copy()
for df in file_list_time:
    df['event'] = [random.randint(0, 1) for _ in range(len(df))]
    df['time_to_event'] = [random.randint(1, 1000) for _ in range(len(df))]


def test_with_function_fix_coef():
    model0 = with_(file_list, model=sm.GLM, endog="y", exog=['age', 'class_Third'], fix_coef=True,
                   family=sm.families.Binomial())

    assert isinstance(model0, tuple) and len(model0) == 2
    assert hasattr(model0[0][0], '__module__') and model0[0][0].__module__.startswith('statsmodels.')
    assert hasattr(model0[1][1], '__module__') and model0[1][1].__module__.startswith('statsmodels.')

def test_with_function():
    model = with_(file_list, model=sm.GLM, endog="y", exog=['age', 'class_Third'], family=sm.families.Binomial())

    assert isinstance(model, list)
    assert hasattr(model[0], '__module__') and model[0].__module__.startswith('statsmodels.')
    assert isinstance(model[0].params["const"], float)

def test_pool_function():
    model = with_(file_list, model=sm.GLM, endog="y", exog=['age', 'class_Third'], family=sm.families.Binomial())
    RR = Rubins_Rules()
    result = RR(model)

    assert isinstance(result, pd.DataFrame)
    assert isinstance(result.loc["const", "logOR"], float)
    assert result["p_value"].between(0, 1).all(), "Not all values are between 0 and 1 for p value"


def test_D3_function():
    RR = Rubins_Rules()

    model0 = with_(file_list, model=sm.GLM, endog="y", exog=['age', 'class_Third'], fix_coef=True,
                   family=sm.families.Binomial())
    model1 = with_(file_list, model=sm.GLM, endog="y", exog=['age', 'fare', 'class_Third'], fix_coef=True,
                   family=sm.families.Binomial())

    result = RR.D3(fit1=model1, fit0=model0)

    assert result == 2.2718679513311635e-05, "p value for llr not the same as R package"
2.271867951397777e-05
# def test_cox_function(): 
#     model = with_(file_list, 
#                 model = sm.PHReg,
#                 endog = 'time_to_event', 
#                 exog = ['age','class_Third'],
#                 add_constant=False,
#                 status='event',
#                 disable_progress_bar=True,
#                 )
#     RR = Rubins_Rules()
#     result = RR(model)
#     assert isinstance(model, list)
#     assert hasattr(model[0], '__module__') and model[0].__module__.startswith('statsmodels.')
#     assert isinstance(result.loc[0, 'SE'], float)

    