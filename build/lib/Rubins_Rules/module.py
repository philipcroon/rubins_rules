import numpy as np
import pandas as pd
from tqdm import tqdm 
from scipy.stats import norm, f
import statsmodels.api as sm

class Rubins_Rules:
        """
        Class for implementing Rubin's Rules to pool results from multiple models.

        Attributes:
            variables (list): The list of variables (column names) present in the models.
            m (int): The number of models (imputations) used for pooling.

        Methods:
            __call__(model_list): Pool results based on Rubin's Rules.
            pool(estimate_list, SE_list): Perform pooling based on Rubin's Rules.
            RR_SE(SE_list, estimate_list, pooled_estimate): Calculate pooled standard error using Rubin's Rules.
        """
        def __init__(self):
            print("initialized")

            """
            Initialize the Rubin's Rules class.
            """
        
        def __call__(self, model_list):
            if is_tuple_sm(model_list):
                model_list = model_list[0]
            if is_statsmodels_result_list(model_list):
                pooled = self.from_models(model_list)
            return pooled
            
       
        def from_models(self, model_list):
            """
            Pool results from multiple models based on Rubin's Rules.

            Parameters:
                model_list (list): A list of statsmodels results in dataframe or the statsmodels objects representing multiple imputations.

            Returns:
                Rubins_Rules: An instance of the Rubins_Rules class with pooled results.
            """
            
            self.variables = model_list[0].params.index
            var_dict = {}
            for var in self.variables:
                est = []
                SE = []
                for i in range(len(model_list)):
                    est.append(model_list[i].params[var])
                    SE.append(model_list[i].bse[var])
                    if len(est)!= len(SE):
                        raise ValueError("Odds and SE not same length")
                self.m = len(est)
                var_dict[var] = self.pool(est, SE)
            pooled_results = pd.DataFrame(var_dict).T
            return pooled_results
        
        
        def pool(self, est:list, SE:list):
            """
            Perform pooling of multiple estimates based on Rubin's Rules.

            Parameters:
                estimate_list (list): A list of estimates from multiple models.
                SE_list (list): A list of standard errors associated with the estimates.

            Returns:
                dict: A dictionary containing the pooled results (estimate, SE, z, Vw, Vb, Lower_CI, Upper_CI, p_value).
            """
            estpool = np.mean(est)
            SEpool, Vw, Vb = self.RR_SE(SE, est, estpool)
            p, z = pvalue(estpool, SEpool)
            Lower_CI, Upper_CI = CI(estpool, SEpool)
            pooled_results = {"logOR": estpool,
                            "SE": SEpool,    
                            "z": z,
                            "Vw": Vw,
                            "Vb": Vb,
                            "Lower_CI": Lower_CI,
                            "Upper_CI": Upper_CI,
                            "p_value": p}
            return pooled_results
            
        def RR_SE(self, SE:list, est_list:list, pooledest): 
            """
            Calculate pooled standard error based on Rubin's Rules.

            Parameters:
                SE_list (list): A list of standard errors from multiple models.
                estimate_list (list): A list of estimates from multiple models.
                pooled_estimate (float): The pooled (mean) estimate from multiple models.

            Returns:
                tuple: A tuple containing the pooled standard error, Vw (within-model variance),
                       and Vb (between-model variance).
            """
            Vw = np.mean(np.square(SE))
            Vb =  np.var(est_list, ddof=1)
            Vtotal = Vw + (1 + 1 / self.m) * Vb
            SEpool = np.sqrt(Vtotal)
            return SEpool, Vw, Vb
        
        def D3(self, fit1, fit0):
            """
            Based on D3 from the R package 
            url: https://github.com/amices/mice/blob/master/R/D3.R

            fit1 is a list of statsmodels results objects for the nested model
            fit0 is a list of statsmodels results for the null model
            """
            if not is_tuple_sm(fit1):
                raise ValueError("The input must be a tuple containing exactly two lists of statsmodels results.")

            if not is_tuple_sm(fit0):
                raise ValueError("The input must be a tuple containing exactly two lists of statsmodels results.")

            # split tuples
            fit0L = fit0[1]
            fit1L = fit1[1]
            fit0 = fit0[0]
            fit1 = fit1[0]

            m = len(fit0)

            # Get pooled results accoring to Rubins Rules
            result_1 = self.from_models(fit1)
            est_list1 = result_1.loc[:, "logOR"]

            result_0 = self.from_models(fit0)
            est_list0 = result_0.loc[:, "logOR"]

            k = len(est_list1) - len(est_list0)

            # For each imputed dataset, calculate the deviance between the two models as fitted
            dev1_M = [-2 * fit.llf for fit in fit1]
            dev0_M = [-2 * fit.llf for fit in fit0]
            dev_M = np.mean(dev0_M) - np.mean(dev1_M)

            # For each imputed dataser, calculate the deviance between models adjusted to coef
            dev1_L = [-2 * fit.llf for fit in fit1L]
            dev0_L = [-2 * fit.llf for fit in fit0L]
            dev_L = np.mean(dev0_L) - np.mean(dev1_L)

            rm = ((m + 1) / (k * (m - 1))) * (dev_M - dev_L)
            
            Dm = dev_L / (k * (1 + rm))

            # Degrees of freedom for F distribution
            v = k * (m - 1)
            if v > 4:
                w = 4 + (v - 4) * ((1 + (1 - 2 / v) * (1 / rm))**2)
            else:
                w = v * (1 + 1 / k) * ((1 + 1 / rm)**2) / 2

            p_value = 1 - f.cdf(Dm, k, w)

            return p_value

def with_(imp, model, endog, exog, y=[], fix_coef=False, disable_progress_bar=False, **kwargs):
    """
    Perform repeated analyses on multiple imputation datasets using the specified model.

    Parameters:
        imp (list): A list of DataFrames representing multiple imputations or MICEForest objects.
        model: A statsmodels model class to use (e.g., sm.GLM, sm.OLS, etc.).
        endog (str): The name of the dependent variable (response variable) in the DataFrames.
        exog (str or list): The name(s) of the independent variables (predictor variables) in the DataFrames.
        y (pd.Series): (optional) If MICEForest objects are given, y should be provided to concatenate to the dataframes.
        fix_coef (bool): Whether to perform additional model fits with fixed coefficients.
        disable_progress_bar (bool): If False, tdqm counts fit over imputations.
        **kwargs: Additional keyword arguments to be passed to the model constructor.

    Returns:
        list or tuple of lists: A list containing the results of the model fit on each imputed dataset.
        If fix_coef is True, returns a tuple of two lists:
        - The first list contains results from models without fixed coefficients.
        - The second list contains results from models with fixed coefficients.
    """

    # If imp is a list of MICEForest objects, convert to dataframes
    if is_mf_result(imp):
        imp, iterations = from_model_list(mice_obj=imp, y=y)

    # Check if imp is a list of DataFrames
    if not isinstance(imp, list) or not all(isinstance(df, pd.DataFrame) for df in imp):
        raise TypeError("imp must be a list of DataFrames representing multiple imputations.")

    # Convert exog to a list if not already
    if not isinstance(exog, list):
        exog = [exog]

    # Check if endog and exog are valid columns in the DataFrames
    for df in imp:
        for e in exog:
            if e not in df.columns:
                raise ValueError(f"Column '{e}' not found in the DataFrames.")
        if endog not in df.columns:
            raise ValueError(f"Column '{endog}' not found in the DataFrames.")

    # Do the repeated analysis, store the result.
    results = []
    for i, dataset_i in tqdm(enumerate(imp, start=1), total=len(imp), disable=disable_progress_bar, desc="Fitting Models"):
        X = dataset_i[exog]
        y = dataset_i[endog]

        # Add an intercept term to the predictors
        X = sm.add_constant(X)

        # Fit the specified model with additional arguments (**kwargs)
        model_result = model(y, X, offset=None, **kwargs).fit()
        results.append(model_result)

    # Update models with coefficients restricted to mean estimates
    if fix_coef:
        mean_estm = {}
        for var in results[0].model.exog_names:
            est = []
            for mod in results:
                est.append(mod.params[var])
            mean_estm[var] = np.mean(est)

        results_offset = []
        for i, dataset_i in tqdm(enumerate(imp, start=1), total=len(imp), disable=disable_progress_bar, desc="Fitting Models restricted"):
            X = dataset_i[exog]
            y = dataset_i[endog]

            # Add an intercept term to the predictors
            X = sm.add_constant(X)

            # Fit the model with coefficients restricted to mean estimates
            mm = X
            X_intercept = X[X.columns[0]].values
            beta = list(mean_estm.values())
            offset = mm @ beta

            # Fit the specified model with additional arguments (**kwargs)
            model_result = model(y, X_intercept, offset=offset, **kwargs).fit()
            results_offset.append(model_result)

        results = (results, results_offset)

    return results


def pvalue(estimate, SE):
    """
    Calculate the p-value based on the estimate and standard error using the z-score.

    Parameters:
        estimate (float): The estimate value.
        SE (float): The standard error associated with the estimate.

    Returns:
        tuple: A tuple containing the p-value and the z-score.
    """
    z = estimate/SE
    p_value = 2 * (1 - norm.cdf(abs(z)))
    return p_value, z
    
def CI(estimate, SE):
        """
        Calculate the confidence interval for the estimate.

        Parameters:
            estimate (float): The estimate value.
            SE (float): The standard error associated with the estimate.

        Returns:
            tuple: A tuple containing the lower and upper bounds of the confidence interval.
        """
        Upper_CI = np.mean(estimate) + 1.96 * SE
        Lower_CI = np.mean(estimate) - 1.96 * SE
        return Lower_CI, Upper_CI

def is_mf_result(obj):
    """
    Check if an object is an instance of a miceforest result class.

    Parameters:
        obj : object
            The object to be checked.

    Returns:
        bool:
            True if the object is an instance of a miceforest result class,
            False otherwise.
    """
    return hasattr(obj, '__module__') and obj.__module__.startswith('miceforest.')

def is_statsmodels_result(obj):
    """
    Check if an object is an instance of a statsmodels result class.

    Parameters:
        obj : object
            The object to be checked.

    Returns:
        bool:
            True if the object is an instance of a statsmodels result class,
            False otherwise.
    """

    return hasattr(obj, '__module__') and obj.__module__.startswith('statsmodels.')


def is_statsmodels_result_list(lst):
    """
    Check if a list contains instances of statsmodels result objects.

    Parameters:
        lst (list): A list of objects to be checked.

    Returns:
        bool:
            True if all objects in the list are instances of statsmodels result classes,
            False otherwise.
    """

    return all(is_statsmodels_result(item) for item in lst)

def is_tuple_sm(tup):
    """
    Check if a tuple contains two lists of statsmodels result objects.

    Parameters:
        tup (tuple): The tuple to be checked.

    Returns:
        bool:
            True if the tuple contains two lists of statsmodels result objects,
            False otherwise.
    """

    # Check if the value is a tuple containing two lists
    if isinstance(tup, tuple) and len(tup) == 2:
        # Check if each element of the lists is a statsmodels result
        return all(is_statsmodels_result_list(lst) for lst in tup)
    else:
        return False
